var express = require('express');
var router = express.Router();
var fs = require('fs');
var cheerio = require('cheerio');
var request = require('request');
var vacanyNameArray = [];
var vacancyDetailsArray = [];
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

//API FOR GETTING THE  VACANCIES
router.get('/crawler',function(req,res){
  //here will come the scraper part
  var url = "http://103.48.109.99:5001/";
  var arr = [];
  //using request i will fetch the vacancyName part
  request(url,function(err,response,html){
    if(!err){
      var $ = cheerio.load(html);
      $(".service-icon").filter(function(index){
        var data = $(this);
        if(index >= 6){
          var vacancyJSON = {
            nameOfVacancy:data.children().first().text(),
          };
          arr.push(vacancyJSON);
        }
      });
      var resJSON = {
        "vacancies" : arr,
      };
      res.send(resJSON);
    }
    else{
      res.send(err);
    }
  });
});

//API FOR GETTING THE VACANCIES and THEIR DETAILS
router.get('/completeInfo',function(req,res){
  var arr = [];
  var url = "http://103.48.109.99:5001/";
  request(url,function(err,resp,html){
    if(!err){
      var $ = cheerio.load(html);
      $(".service-icon").filter(function(index){
        var data = $(this);
        if(index > 6){
          //console.log(data.children().first().text());
          vacanyNameArray.push(data.children().first().text());
          console.log(vacanyNameArray);
        }
      });

      $(".service-desc").filter(function(index){
        var data = $(this);
        if(index > 5){
          //console.log(data.children().first().text());
          vacancyDetailsArray.push(data.children().first().text());
        }
      });
    }
    else{
      res.end(err);
    }
    for(var i=0; i< vacanyNameArray.length ; i++){
      var json = {
        'vacancyName':vacanyNameArray[i],
        'vacancyInfo':vacancyDetailsArray[i]
      };
      arr.push(json);
    }
    var resJSON = {
      "vacancies":arr
    }
    res.send(resJSON);
  });
});


//API FOR GETTING THE INFO ABOUT THE VACANCY
router.get('/vacancyInfo',function(req,res){
  var url = "http://103.48.109.99:5001/";
  var detailsArr = [];
  request(url,function(err,response,html){
    if(!err){
      var $ = cheerio.load(html);
      $(".service-desc").filter(function(index){
        var data = $(this);
        if(index >= 5){
        //console.log(index);
        //console.log(data.children().first().text());
        var detailJSON = {
          "info":data.children().first().text()
        };
        detailsArr.push(detailJSON);
      }
      });
    }
    else{
      res.send(err);
    }
    var resJSON = {
      "vacancyDetails":detailsArr
    };
    res.send(resJSON.vacancyDetails);
  });

});
module.exports = router;
